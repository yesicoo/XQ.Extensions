﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using XQ.Extensions;

namespace XQ
{
    /// <summary>
    /// String类型的扩展方法
    /// </summary>
    public static class StringExtensions
    {
        #region 是否为Int型字符串
        /// <summary>
        /// 是否为Int型字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsInt(this string value)
        {
            int outValue = 0;
            return int.TryParse(value, out outValue);
        } 
        #endregion

        #region 转换成Int
        /// <summary>
        /// 转换成Int
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this string value)
        {
           return int.Parse(value);
        } 
        #endregion

        #region 是否为Double型字符串
        /// <summary>
        /// 是否为Double型字符串
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsDouble(this string value)
        {
            double outValue = 0;
            return double.TryParse(value, out outValue);
        }
        #endregion

        #region  转换成Double
        /// <summary>
        /// 转换成Double
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDouble(this string value)
        {
          return  double.Parse(value);
        }
        #endregion

        #region 转换成DateTime格式
        /// <summary>
        /// 转换成DateTime格式
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string value) {
          return  DateTime.Parse(value);
        }
        #endregion

        #region 字符串是否为空或者Null
        /// <summary>
        /// 字符串是否为空或者Null
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        } 
        #endregion

        #region 格式化字符串
        /// <summary>
        /// 格式化字符串
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args">占位参数</param>
        /// <returns></returns>
        public static string FormatWith(this string format, params object[] args)
        {
            return string.Format(format, args);
        } 
        #endregion

        #region 验证正则是否匹配
        /// <summary>
        /// 验证正则是否匹配
        /// </summary>
        /// <param name="value"></param>
        /// <param name="pattern">正则表达式</param>
        /// <returns></returns>
        public static bool IsMatch(this string value, string pattern)
        {
            if (value.IsNullOrEmpty()) return false;
            else return Regex.IsMatch(value, pattern);
        } 
        #endregion

        #region 获取正则匹配结果
        /// <summary>
        /// 获取正则匹配结果
        /// </summary>
        /// <param name="value"></param>
        /// <param name="pattern">正则表达式</param>
        /// <returns></returns>
        public static string Match(this string value, string pattern)
        {
            if (value.IsNullOrEmpty()) return string.Empty;
            return Regex.Match(value, pattern).Value;
        } 
        #endregion

        #region 取汉字拼音的首字母
        /// <summary>
        /// 取汉字拼音的首字母
        /// </summary>
        /// <param name="UnName">汉字</param>
        /// <returns>首字母</returns>
        public static string GetPinYinSX(this string UnName)
        {
            int i = 0;
            ushort key = 0;
            string strResult = string.Empty;

            Encoding unicode = Encoding.Unicode;
            Encoding gbk = Encoding.GetEncoding(936);
            byte[] unicodeBytes = unicode.GetBytes(UnName);
            byte[] gbkBytes = Encoding.Convert(unicode, gbk, unicodeBytes);
            while (i < gbkBytes.Length)
            {
                if (gbkBytes[i] <= 127)
                {
                    strResult = strResult + (char)gbkBytes[i];
                    i++;
                }
                #region 生成汉字拼音简码,取拼音首字母
                else
                {
                    key = (ushort)(gbkBytes[i] * 256 + gbkBytes[i + 1]);
                    if (key >= '\uB0A1' && key <= '\uB0C4')
                    {
                        strResult = strResult + "A";
                    }
                    else if (key >= '\uB0C5' && key <= '\uB2C0')
                    {
                        strResult = strResult + "B";
                    }
                    else if (key >= '\uB2C1' && key <= '\uB4ED')
                    {
                        strResult = strResult + "C";
                    }
                    else if (key >= '\uB4EE' && key <= '\uB6E9')
                    {
                        strResult = strResult + "D";
                    }
                    else if (key >= '\uB6EA' && key <= '\uB7A1')
                    {
                        strResult = strResult + "E";
                    }
                    else if (key >= '\uB7A2' && key <= '\uB8C0')
                    {
                        strResult = strResult + "F";
                    }
                    else if (key >= '\uB8C1' && key <= '\uB9FD')
                    {
                        strResult = strResult + "G";
                    }
                    else if (key >= '\uB9FE' && key <= '\uBBF6')
                    {
                        strResult = strResult + "H";
                    }
                    else if (key >= '\uBBF7' && key <= '\uBFA5')
                    {
                        strResult = strResult + "J";
                    }
                    else if (key >= '\uBFA6' && key <= '\uC0AB')
                    {
                        strResult = strResult + "K";
                    }
                    else if (key >= '\uC0AC' && key <= '\uC2E7')
                    {
                        strResult = strResult + "L";
                    }
                    else if (key >= '\uC2E8' && key <= '\uC4C2')
                    {
                        strResult = strResult + "M";
                    }
                    else if (key >= '\uC4C3' && key <= '\uC5B5')
                    {
                        strResult = strResult + "N";
                    }
                    else if (key >= '\uC5B6' && key <= '\uC5BD')
                    {
                        strResult = strResult + "O";
                    }
                    else if (key >= '\uC5BE' && key <= '\uC6D9')
                    {
                        strResult = strResult + "P";
                    }
                    else if (key >= '\uC6DA' && key <= '\uC8BA')
                    {
                        strResult = strResult + "Q";
                    }
                    else if (key >= '\uC8BB' && key <= '\uC8F5')
                    {
                        strResult = strResult + "R";
                    }
                    else if (key >= '\uC8F6' && key <= '\uCBF9')
                    {
                        strResult = strResult + "S";
                    }
                    else if (key >= '\uCBFA' && key <= '\uCDD9')
                    {
                        strResult = strResult + "T";
                    }
                    else if (key >= '\uCDDA' && key <= '\uCEF3')
                    {
                        strResult = strResult + "W";
                    }
                    else if (key >= '\uCEF4' && key <= '\uD188')
                    {
                        strResult = strResult + "X";
                    }
                    else if (key >= '\uD1B9' && key <= '\uD4D0')
                    {
                        strResult = strResult + "Y";
                    }
                    else if (key >= '\uD4D1' && key <= '\uD7F9')
                    {
                        strResult = strResult + "Z";
                    }
                    else
                    {
                        strResult = strResult + "?";
                    }
                    i = i + 2;
                }
                #endregion
            }
            return strResult.ToLower();
        }

        #endregion

        #region 汉字转换成全拼的拼音
        /// <summary>
        /// 汉字转换成全拼的拼音
        /// </summary>
        /// <param name="value">汉字字符串</param>
        /// <returns>转换后的拼音字符串</returns>
        public static string GetPinYin(this string value)
        {
            Regex reg = new Regex("^[\u4e00-\u9fa5]$");//验证是否输入汉字
            byte[] arr = new byte[2];
            string pystr = "";
            int asc = 0, M1 = 0, M2 = 0;
            char[] mChar = value.ToCharArray();//获取汉字对应的字符数组
            for (int j = 0; j < mChar.Length; j++)
            {
                //如果输入的是汉字
                if (reg.IsMatch(mChar[j].ToString()))
                {
                    arr = System.Text.Encoding.Default.GetBytes(mChar[j].ToString());
                    M1 = (short)(arr[0]);
                    M2 = (short)(arr[1]);
                    asc = M1 * 256 + M2 - 65536;
                    if (asc > 0 && asc < 160)
                    {
                        pystr += mChar[j];
                    }
                    else
                    {
                        switch (asc)
                        {
                            case -9254:
                                pystr += "Zhen"; break;
                            case -8985:
                                pystr += "Qian"; break;
                            case -5463:
                                pystr += "Jia"; break;
                            case -8274:
                                pystr += "Ge"; break;
                            case -5448:
                                pystr += "Ga"; break;
                            case -5447:
                                pystr += "La"; break;
                            case -4649:
                                pystr += "Chen"; break;
                            case -5436:
                                pystr += "Mao"; break;
                            case -5213:
                                pystr += "Mao"; break;
                            case -3597:
                                pystr += "Die"; break;
                            case -5659:
                                pystr += "Tian"; break;
                            default:
                                for (int i = (PinYin.getValue.Length - 1); i >= 0; i--)
                                {
                                    if (PinYin.getValue[i] <= asc) //判断汉字的拼音区编码是否在指定范围内
                                    {
                                        pystr += PinYin.getName[i];//如果不超出范围则获取对应的拼音
                                        break;
                                    }
                                }
                                break;
                        }
                    }
                }
                else//如果不是汉字
                {
                    pystr += mChar[j].ToString();//如果不是汉字则返回
                }
            }
            return pystr.ToLower();//返回获取到的汉字拼音
        }
        #endregion

        #region  字符串截断后几位
        /// <summary>
        /// 字符串截断后几位
        /// </summary>
        /// <param name="value"></param>
        /// <param name="CutLenght">截断位数，默认1</param>
        /// <returns></returns>
        public static string CutLast(this string value, int CutLenght = 1)
        {
            if (value.IsNullOrEmpty()) return null;
            if (value.Length <= CutLenght) return null;
            return value.Substring(0, value.Length - CutLenght);
        } 
        #endregion

        #region 转换成List
        /// <summary>
        /// 转换成List
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="separator">分隔符 默认 ; </param>
        /// <returns></returns>
        public static List<string> SplitToList(this string value, params string[] separators)
        {
            List<string> l = new List<string>();
            string[] values = null;
            if (separators.Length == 0)
            {

                values = value.Split(';');
            }
            else
            {
                values = value.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            }
            foreach (string item in values)
            {
                if (string.IsNullOrEmpty(item.Trim()))
                    continue;
                l.Add(item);
            }
            return l;
        } 
        #endregion

        #region 将字符串按长度切割后加省略号
        /// <summary>
        /// 扩展方法:将字符串按长度切割
        /// </summary>
        /// <param name="oldStr"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string Cut(this string oldStr, int len = 10)
        {
            var oldStrLen = oldStr.Length;
            if (oldStrLen <= len)
            {
                return oldStr;
            }
            else
            {
                return oldStr.Substring(0, len - 3) + "...";
            }
        } 
        #endregion

        #region   字符串长度区分中英文截取后加省略号
        /// <summary>   
        /// 截取文本，区分中英文字符，中文算两个长度，英文算一个长度
        /// </summary>
        /// <param name="str">待截取的字符串</param>
        /// <param name="length">需计算长度的字符串</param>
        /// <returns>string</returns>
        public static string Cuts(this string str, int length = 10)
        {
            string temp = str;
            int j = 0;
            int k = 0;
            for (int i = 0; i < temp.Length; i++)
            {
                j = Regex.IsMatch(temp.Substring(i, 1), @"[\u4e00-\u9fa5]+") ? j + 2 : j + 1;
                k = j <= length ? k + 1 : k;
                if (j > length) return temp.Substring(0, k) + "...";
            }
            return temp;
        }
        #endregion

        #region 将字符串逆转
        /// <summary>
        /// 扩展方法：将字符串逆转
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static string Reverse(this string oldStr)
        {
            char[] temp = oldStr.ToCharArray();
            Array.Reverse(temp);
            return new string(temp);
        } 
        #endregion

        #region 字符串验证

        /// <summary>
        /// 扩展方法:判断字符串是否只由数字组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsNumber(this string oldStr)
        {
            return oldStr.IsMatch(@"^[0-9]+$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否只由数字或字母组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsNumberOrLetter(this string oldStr)
        {
            return oldStr.IsMatch( @"^[A-Za-z0-9]+$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否只由数字或字母或汉字组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsNumberOrLetterOrChinese(this string oldStr)
        {
            return oldStr.IsMatch( @"[a-zA-Z0-9\u4e00-\u9fa5]{1,50}");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否只由汉字组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsChinese(this string oldStr)
        {
            return oldStr.IsMatch( @"^[\u4e00-\u9fa5]+$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否是Email
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsEmail(this string oldStr)
        {
            return oldStr.IsMatch( @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否是身份证号
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsIDCard(this string oldStr)
        {
            return oldStr.IsMatch( @"^(^\d{18}$)|(^\d{15}$)");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否是手机号
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsMobile(this string oldStr)
        {
            return oldStr.IsMatch(@"(^189\d{8}$)|(^13\d{9}$)|(^15\d{9}$)");
        }
        #endregion

        #region 字符串加密
        /// <summary>
        /// 字符串加密成字符串
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns>加密后的字符串</returns>
        public static string CreatePassword(this string oldStr)
        {
            /*NOTE:
             * 一个密码字符串由数字和母组成
             * STEP1: 产生一个key,范围在11-99之间
             * STEP2: 移位值=(字符串长度+key)%(key个十位相加值)
             * STEP3: 数字=数字左移移位值位，字母=字母右移位值位
             * STEP4: key转成十六进制(2位)加在结果后面
            */
            int key, displacement;
        repeat: key = new Random().Next(11, 99);
            displacement = (oldStr.Length + key) % (key % 10 + key / 10);
            if (displacement == 0) goto repeat;
            string result = "";

            char[] items = oldStr.ToCharArray();
            foreach (var i in items)
            {
                if (i - 48 >= 0 && i - 48 <= 9)
                {
                    if ((i - displacement) >= '0')
                    {
                        result += (char)(i - displacement);
                    }
                    else
                    {
                        result += (char)(58 - (48 - (i - displacement)));
                    }
                }
                else if (i >= 'a' && i <= 'z')
                {
                    result += (char)((i - 97 + displacement) % 26 + 97);
                }
                else if (i >= 'A' && i <= 'Z')
                {
                    result += (char)((i - 65 + displacement) % 26 + 65);
                }
            }
            return result + key.ToString("X2");
        }

        /// <summary>
        /// 字符串解密成字符串
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns>解密后的字符串</returns>
        public static string RemovePassword(this string oldStr)
        {
            string handleStr = oldStr.Substring(0, oldStr.Length - 2);
            int key = Int32.Parse(oldStr.Substring(oldStr.Length - 2), System.Globalization.NumberStyles.HexNumber);
            int dpt = (handleStr.Length + key) % (key % 10 + key / 10);
            char[] items = handleStr.ToCharArray();
            string result = "";
            foreach (var i in items)
            {
                if (i - 48 >= 0 && i - 48 <= 9)
                {
                    if (i + dpt <= '9')
                        result += (char)(i + dpt);
                    else
                        result += (char)(48 + ((i + dpt) - 58));
                }
                else if (i >= 'a' && i <= 'z')
                {
                    if (i - dpt >= 'a')
                    {
                        result += (char)(i - dpt);
                    }
                    else
                    {
                        result += (char)(123 - Math.Abs(97 - (i - dpt)));
                    }
                }
                else if (i >= 'A' && i <= 'Z')
                {
                    if (i - dpt >= 'A')
                    {
                        result += (char)(i - dpt);
                    }
                    else
                    {
                        result += (char)(81 - Math.Abs(65 - (i - dpt)));
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 扩展方法:数字型字符串加密成数字
        /// </summary>
        /// <param name="oldStr">最大9位数</param>
        /// <param name="key">两位数密钥</param>
        /// <returns></returns>
        public static string ToPwd(this string oldStr, int key)
        {
            /// 算法：
            /// Step1: oldStr(oldStr小于等于9位) * key各位之和(key小于等于99)
            /// Step2: 再将其与(key*key)异或
            /// Step3: 再将结果倒序输出
            /// 例如: oldStr="12345678" ,key=56
            /// Step1: 12345678 * 11 = 135802458
            /// Step2: 135802458^(56*56)=135799322
            /// 结果: 223997531
            if (!oldStr.IsNumber())
            {
                return "Inviad String";
            }

            if (oldStr.Length > 10)
            {
                return "Too Long";
            }

            int keyer = key / 10 + key % 10;
            string step1 = (Convert.ToInt64(oldStr) * keyer).ToString();
            string step2 = (Convert.ToInt64(step1) ^ (key * key)).ToString();
            string result = step2.Reverse();
            return result;
        }

        /// <summary>
        /// 数字字符串解密成数字算法
        /// </summary>
        /// <param name="oldStr"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DePwd(this string oldStr, int key)
        {
            int keyer = key / 10 + key % 10;
            string step1 = oldStr.Reverse();
            string step2 = (Convert.ToInt64(step1) ^ (key * key)).ToString();
            string result = (Convert.ToInt64(step2) / keyer).ToString();
            return result;
        }

        /// <summary>
        /// 扩展方法:将字符以MD5方式加密
        /// </summary>
        /// <param name="oldStr">加密字符串</param>
        /// <returns>加密结果</returns>
        public static string GetMD5(this string input, string str = "")
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString() + str;
        }
        #endregion

        #region base64加密，可用于Url加密
        /// <summary>
        /// base64加密,可用于Url加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToBase64Str(this string str)
        {
            return Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(str)).Replace("+", "%2B");
        } 
        #endregion

        #region base64解密，可用于Url解密
        /// <summary>
        /// base64解密，可用于Url解密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string DeBase64Str(this string str)
        {
            return System.Text.Encoding.Default.GetString(Convert.FromBase64String(str.Replace("%2B", "+")));
        } 
        #endregion

        #region 替换字符串中的html代码 
        /// <summary>
        /// 替换字符串中的html代码 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ReplaceHtmlString(this string str)
        {
            return Regex.Replace(str, @"<[^>]+>", "");
        } 
        #endregion

        #region DES加密
        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="code">加密字符串</param>
        /// <param name="key">密钥</param>
        /// <returns></returns>
        public static string DesEncrypt(this string code, string key)
        {
            string iv = key;
            return DesEncrypt(code, key, iv);
        } 
       

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="code">加密字符串</param>
        /// <param name="key">密钥</param>
        /// <param name="iv">初始化向量</param>
        /// <returns></returns>
        public static string DesEncrypt(string code, string key, string iv)
        {
            try
            {
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.Default.GetBytes(code);
                des.Key = ASCIIEncoding.ASCII.GetBytes(key);
                des.IV = ASCIIEncoding.ASCII.GetBytes(iv);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                StringBuilder ret = new StringBuilder();
                foreach (byte b in ms.ToArray())
                {
                    ret.AppendFormat("{0:X2}", b);
                }
                ms.Dispose();
                cs.Dispose();
                //ret.ToString();
                return ret.ToString();
            }
            catch (Exception)
            {

                return code;
            }

        }
        #endregion

        #region DES解密,解密失败返回源串
        /// <summary>
        /// DES解密,解密失败返回源串
        /// </summary>
        /// <param name="code">解密字符串</param>
        /// <param name="key">密钥</param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DesDecrypt(this string code, string key)
        {
            string iv = key;
            return DesDecrypt(code, key, iv);
        }


        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="code">解密字符串</param>
        /// <param name="key">密钥</param>
        /// <param name="iv">初始化向量</param>
        /// <returns></returns>
        public static string DesDecrypt(string code, string key, string iv)
        {
            try
            {
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = new byte[code.Length / 2];
                for (int x = 0; x < code.Length / 2; x++)
                {
                    int i = (Convert.ToInt32(code.Substring(x * 2, 2), 16));
                    inputByteArray[x] = (byte)i;
                }
                des.Key = ASCIIEncoding.ASCII.GetBytes(key);
                des.IV = ASCIIEncoding.ASCII.GetBytes(iv);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                cs.Dispose();
                StringBuilder ret = new StringBuilder();
                return System.Text.Encoding.Default.GetString(ms.ToArray());
            }
            catch (Exception)
            {

                return code;
            }

        }
        
        #endregion
    }
}
