﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ
{
    public static class ListT_Extensions
    {
        /// <summary>
        /// List转换为String
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="value">值</param>
        /// <param name="separators">间隔符</param>
        /// <returns></returns>
        public static string ToListString<T>(this List<T> value, string separators = ";")
        {
            StringBuilder sb = new StringBuilder();
            foreach (T item in value)
            {
                sb.Append(item.ToString()).Append(separators);
            }
            return sb.ToString();
        }
    }
}
