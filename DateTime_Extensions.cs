﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XQ
{
  public static  class DateTime_Extensions
    {
      public static string ToNormString(this DateTime value)
      {
            return value.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
