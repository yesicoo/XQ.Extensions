﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace XQ
{
    public static class DoubleExtensions
    {
        /// <summary>
        /// 四舍五入保留N位数有效小数
        /// </summary>
        /// <param name="value"></param>
        /// <param name="Digits">有效小数位数,默认2</param>
        /// <returns></returns>
        public static double MathRound(this double value, int Digits = 2)
        {
            return Math.Round(value, Digits);
        }
        /// <summary>
        /// 获取大写人民币
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToMoneyUpper(this double value)
        {
            string s = value.ToString("#L#E#D#C#K#E#D#C#J#E#D#C#I#E#D#C#H#E#D#C#G#E#D#C#F#E#D#C#.0B0A");//d + "\n" +
            string d = Regex.Replace(s, @"((?<=-|^)[^1-9]*)|((?'z'0)[0A-E]*((?=[1-9])|(?'-z'(?=[F-L\.]|$))))|((?'b'[F-L])(?'z'0)[0A-L]*((?=[1-9])|(?'-z'(?=[\.]|$))))", "${b}${z}");
            return Regex.Replace(d, ".", delegate(Match m) { return "负元空零壹贰叁肆伍陆柒捌玖空空空空空空空分角拾佰仟萬億兆京垓秭穰"[m.Value[0] - '-'].ToString(); });
        }


       
    }
}
